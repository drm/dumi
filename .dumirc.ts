import { defineConfig } from 'dumi';

const isProd = process.env.NODE_ENV === 'production';
console.log("isProd", isProd);

export default defineConfig({
    // history: {
    //     type: "hash"
    // },
    //dailycomponent
    base: isProd ? '/dumi' : '/',
    publicPath: isProd ? '/dumi/' : '/',
    runtimePublicPath: {},
    devtool: isProd ? false : 'source-map',//生成map文件
    themeConfig: {
        name: 'dailyCom',
        nav: [
            {
                title: "2d组件",
                link: "/guide"
            },
            {
                title: "3d组件",
                link: "/3d"
            },
        ],
        description: "日程部门业务沉淀文档",
        footer: false
    },
});
