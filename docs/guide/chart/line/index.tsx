/*
 * @Author: xiaosihan 
 * @Date: 2022-07-11 07:42:43 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2022-11-20 17:51:15
 */

import { useEffect, useRef, useState } from "react";
import styles from "./index.module.less";
import ReactEcharts from 'echarts-for-react';
import clsx from "clsx";
import { getOptions } from "./instants";
import { threeUtils } from "three-base";

const provinceList = {
    0: [0, 0, 0, 0, 0, 78929, 39351, 45391, 36870, 42129],
    1: [0, 0, 0, 0, 0, 52021, 19922, 22408, 19332, 23050],
    2: [0, 0, 0, 0, 0, 18919, 12297, 13487, 8766, 9614],
    3: [0, 0, 0, 0, 0, 3235, 3081, 4115, 3744, 3856],
    4: [0, 0, 0, 0, 0, 2672, 2477, 2341, 2262, 2492],
    5: [0, 0, 0, 0, 0, 1432, 1574, 3040, 2766, 3117],
};

const titles = ["", '中药饮片限复方使用', '单次就诊中药饮片单味不予支付', '中药饮片单味不予支付', '单次就诊诊疗项目重复收费', '诊疗项目重复收费'];

const Line = () => {

    const [active, setActive] = useState<0 | 1 | 2 | 3 | 4 | 5>(0);
    const [option, setOption] = useState<any>(null);
    const chart = useRef<any>({});
    const index = useRef<number>(0);

    // useEffect(() => {
    //     setOption(getOptions(provinceList[active]))
    // }, [])

    useEffect(() => {
        setOption(getOptions())
    }, []);



    return (
        <div className={styles.left3}>
            {
                option && <ReactEcharts option={option} ref={chart} />
            }
        </div>
    );
}

export default Line;