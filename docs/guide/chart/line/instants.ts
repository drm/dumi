import * as echarts from 'echarts';

export const getOptions = () => {
    return {
        color: ['#1bc2f6', '#12ecb7'],
        tooltip: {
            trigger: 'axis',
            backgroundColor: 'rgba(7,46,70, .8)',
            borderColor: '#099bbf',
            textStyle: {
                color: '#fff'
            },
            axisPointer: {
                type: 'line',
                lineStyle: {
                    color: 'rgba(50, 216, 205, 1)'
                },
            },

        },
        grid: {
            left: '2%',
            right: '4%',
            bottom: '15%',
            top: '40',
            containLabel: true,
        },
        legend: {
            // icon: 'rect',
            // orient: 'horizontal',
            left: 'right',
            // itemWidth: 12,
            // itemHeight: 12,
            // formatter: ['{a|{name}}'].join('\n'),
            textStyle: {
                fontSize: 12,
                color: '#6A93B9',
                height: 8,
                rich: {
                    a: {
                        verticalAlign: 'bottom',
                    },
                },
            },
            data: ['2021', '2022'],
        },
        xAxis: {
            type: 'category',
            axisLine: {
                lineStyle: {
                    color: '#616d87',
                },
            },

            axisLabel: {
                interval: 1,
                fontSize: 12,
                color: '#6A93B9',
                // formatter: (v: any) => {
                //     return "违规人次";
                //     let name = v.split('年')[1];
                //     return name;
                // }
            },
            axisTick: {
                show: false,
            },
            splitLine: {
                lineStyle: {
                    color: 'rgba(255, 255, 255, 0.15)',
                    type: 'dashed', // dotted 虚线
                },
            },
            data: ['医疗机构', '零售药店', '经办机构', '参保单位', '个人', '医疗服务人员', '药品生产企业', '药品配送企业', '其他'],
        },
        yAxis: [{
            name: '被举报对象数量',
            offset: 0,
            type: 'value',
            min: 0,
            minInterval: 1,
            nameLocation: 'end',
            nameTextStyle: {
                fontSize: 12,
                color: '#BEC5D9',
                align: 'left',
            },
            splitLine: {
                lineStyle: {
                    color: 'rgba(255, 255, 255, 0.15)',
                    type: 'dashed', // dotted 虚线
                },
            },
            splitArea: { show: false },
            axisLine: {
                show: true,
                lineStyle: {
                    color: 'rgba(255, 255, 255, 0.15)',
                    type: 'dashed', // dotted 虚线
                },
            },
            axisTick: {
                show: false,
            },
            axisLabel: {
                fontSize: 12,
                fontFamily: 'Bebas',
                color: '#6A93B9',
            },
        }, {
            name: '占比(%)',
            nameTextStyle: {
                color: '#BEC5D9',
            },
            type: 'value',
            min: 0,
            max: 100,
            splitLine: {
                show: false
            },

            axisTick: {
                show: false
            },
            axisLine: {
                lineStyle: {
                    color: 'rgba(255, 255, 255, 0.15)',
                    type: 'dashed', // dotted 虚线
                },
                show: true,
            },
            axisLabel: {
                color: '#BEC5D9',
                // formatter: '{value}ms'
            },
        }],
        series: [
            {
                type: 'bar',
                barWidth: 16,
                name: '被举报对象数量', // 图例对应类别
                data: [34, 30, 0, 0, 0, 0, 0, 0, 0], // 纵坐标数据
                itemStyle: {
                    normal: {
                        color: function () {
                            return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                offset: 0,
                                color: '#1bbef5'
                            },
                            {
                                offset: 1,
                                color: 'rgba(26,255,255,0.1)'
                            }
                            ], false);
                        }
                    }
                },
            },
            {
                type: 'line',
                showSymbol: false,
                yAxisIndex: 1,
                name: '占比', // 图例对应类别
                data: [53.16, 46.88, 0, 0, 0, 0, 0, 0, 0],
            },
        ],
    };

}