/*
 * @Author: xiaosihan 
 * @Date: 2022-07-11 07:42:43 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2022-11-20 17:48:05
 */

import { useEffect, useRef, useState } from "react";
import styles from "./index.module.less";
import clsx from "clsx";
import { threeUtils, Transition } from "three-base";
import UpImg from './img/up.png';
import DownImg from './img/down.png';
import utils from "../utils";


const Left2 = () => {
    const isHover = useRef(false);
    const dom = useRef<HTMLDivElement | null>(null);
    const dts = [
        { city: '中药饮片限复方使用', num: utils.thousandth(136733), rate: '19%', up: true, type: 1 },
        { city: '单次就诊中药饮片单味不予支付', num: utils.thousandth(63083), rate: '10%', up: true, type: 2 },
        { city: '中药饮片单味不予支付', num: utils.thousandth(18031), rate: '3%', up: true, type: 3 },
        { city: '单次就诊诊疗项目重复收费', num: utils.thousandth(12244), rate: '10%', up: true, type: 4 },
        { city: '诊疗项目重复收费', num: utils.thousandth(11929), rate: '13%', up: true, type: 5 },
    ]

    const mouseScroll = (e: any) => {
        const { scrollTop, scrollHeight, clientHeight } = e.target;
        if (isHover.current) {
            if (scrollTop === 10) {
                e.target.scrollTop = (scrollHeight / 2) + 10;
            } else if (scrollTop > (scrollHeight / 2) - clientHeight) {
                e.target.scrollTop = (scrollHeight / 2) - clientHeight;
            }
        } else {
            if (scrollTop === 0) {
                e.target.scrollTop = (scrollHeight / 2);
            } else if (scrollTop > (scrollHeight / 2)) {
                e.target.scrollTop = scrollTop - (scrollHeight / 2);
            }
        }
    }


    useEffect(() => {
        const transition = new Transition({ scrollTop: 0 });
        transition.onChange(({ scrollTop }) => {
            if (dom.current) {
                dom.current.scrollTop = scrollTop;
            }
        });

        const scroll = (e: any) => {

            const { accumulate } = e;

            if (isHover.current) { return; }

            // 60s执行一次
            if (accumulate % 40 === 0) {
                if (dom.current) {

                    const { scrollTop, scrollHeight } = dom.current;
                    if (!isNaN(scrollTop)) {

                        // 没有选中时的滚动计算
                        if (scrollTop >= scrollHeight / 2) {
                            transition.reset({
                                scrollTop: scrollTop % (scrollHeight / 2),
                            });
                            const index = Math.floor((scrollTop % (scrollHeight / 2)) / 40);
                            transition.set({ scrollTop: (index + 1) * 40 });
                        } else {
                            transition.reset({ scrollTop });
                            const index = Math.floor((scrollTop + 5) / 40);
                            transition.set({ scrollTop: (index + 1) * 40 });
                        }
                    }
                }
            }
        }
        threeUtils.addEventListener("requestAnimationFrame", scroll);

        return () => {
            threeUtils.removeEventListener("requestAnimationFrame", scroll);

        }

    }, []);

    return (

        <div
            className={styles.position}

        >
            <div className={clsx(styles.table_item)}>
                <div className={styles.item1}>排名</div>
                <div className={styles.item2}>规则名称</div>
                <div className={styles.item3Center}>违规人次</div>
                <div className={styles.item5}>
                    月涨幅
                </div>
            </div>
            <div className={styles.table}
                ref={dom}
                // onScroll={mouseScroll}
                onMouseOver={() => (isHover.current = true)}
                onMouseLeave={() => (isHover.current = false)}
            >
                {
                    [...dts, ...dts].map(({ city, num, rate, up }, i) => (
                        <div key={i} className={styles.table_item}>
                            <div className={styles.item1}>{i + 1}</div>
                            <div className={styles.item2}>{city}</div>
                            <div className={styles.item3}>{num}</div>
                            <div className={styles.item4}>
                                <div className={styles.width}>
                                    <span>{rate}</span>
                                    {
                                        rate === '0%' ? <span style={{ visibility: 'hidden' }}>&nbsp;&nbsp;&nbsp;</span> : <img src={up ? UpImg : DownImg} />
                                    }
                                </div>
                            </div>
                        </div>
                    ))
                }
            </div>
        </div>

    );


}

export default Left2;