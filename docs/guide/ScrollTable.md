---
title: 列表无限滚动
order: 3
---
```jsx
/**
 * background: '#112652'
 */
import ScrollTable from './ScrollTable/index.tsx';

export default () => <ScrollTable />;
```

> 参考antd Table传入数据格式
