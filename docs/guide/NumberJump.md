---
group: 数字
title: 数字滚动
order: 2
---
```jsx
import NumberJump from './NumberJump/NumberJump.tsx';

export default () => <div>
			<NumberJump
                                value={123}
                                color="#000"
                                fontSize={24}
                            />
		     </div>;
```

| 参数     | 说明     | 类型   | 默认值   |
| -------- | -------- | ------ | -------- |
| value    | 当前值   | number | 0        |
| color    | 数字颜色 | string | ``#fff`` |
| fontSize | 字体大小 | number | 12       |
