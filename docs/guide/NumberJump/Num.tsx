/*
 * @Author: xiaosihan 
 * @Date: 2022-10-23 14:11:54 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2022-10-23 15:25:15
 */


import clsx from "clsx";
import { useEffect, useRef, useState } from "react";
import { Transition } from "three-base";
import styles from "./num.module.less";

interface Iprops {
    value: number,
    color: string,
    fontSize: number
}

export default function Num(props: Iprops) {

    const scrollNum = useRef<HTMLDivElement | null>(null);

    const transition = useRef<Transition>(new Transition({ top: 0 }).setDuration(3000));

    useEffect(() => {
        transition.current.onChange(({ top }) => {
            if (scrollNum.current) {
                scrollNum.current.style.top = `${-top}px`;
            }
        });
    }, []);

    useEffect(() => {

        transition.current.set({ top: props.value * props.fontSize });

    }, [props.value, props.fontSize]);

    return (
        <span
            className={clsx(styles.num, "number_jump_num")}
            style={{ width: `${props.fontSize / 1.5}px`, height: `${props.fontSize}px` }}
        >
            <span
                className={clsx(styles.jump_scroll_num, "number_jump_scroll_num")}
                ref={scrollNum}
                style={{ width: `${props.fontSize / 1.5}px`, height: `${props.fontSize}px` }}
            >
                {
                    new Array(10).fill(0).map((v, i) => {
                        return (
                            <span
                                key={i}
                                className={clsx(styles.jump_scroll_num_item, "number_jump_scroll_num_item")}
                                style={{
                                    fontSize: `${props.fontSize}px`,
                                    lineHeight: `${props.fontSize}px`,
                                    color: props.color,
                                    height: `${props.fontSize}px`
                                }}
                            >{i}</span>
                        )
                    })
                }
            </span>
        </span >
    );

}