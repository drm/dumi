/*
 * @Author: xiaosihan 
 * @Date: 2022-10-23 14:27:03 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2022-10-23 15:43:31
 */


import clsx from "clsx";
import Num from "./Num";
import styles from "./numberJump.module.less";

interface Iprops {
    value: number | string,
    color?: string,
    fontSize?: number,
    className?: string
}

export default function NumberJump(props: Iprops) {

    const { fontSize = 20, color = "#ffffff" } = props;

    return (
        <span className={clsx(styles.numberJump, props.className)}>

            {
                props.value === 'NaN' ? '-' :
                    String(props.value).split("").map((v, i) => {
                        return (
                            <span
                                key={i}
                                className="string_container"
                                style={{
                                    display: "inline-block",
                                    width: `${fontSize / 1.5}px`,
                                    height: `${fontSize}px`,
                                    lineHeight: `${fontSize}px`
                                }}
                            >
                                {
                                    !isNaN(Number(v)) ?
                                        <Num value={Number(v)} color={color} fontSize={fontSize} />
                                        :
                                        <span
                                            className="string"
                                            style={{
                                                display: "inline-block",
                                                position: "relative",
                                                color: props.color,
                                                width: `${fontSize / 1.5}px`,
                                                height: `${fontSize}px`,
                                                fontSize: `${fontSize}px`,
                                                lineHeight: `${fontSize}px`
                                            }}
                                        >{v}</span>
                                }
                            </span>
                        );
                    })
            }
        </span>
    );

}