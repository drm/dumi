
---
title: 页面名称
order: 0
nav:
  title: 导航名称
  order: 0
group:
  title: 自定义分组名称
  order: 0
---


### Icon

| 属性  | 说明      | 类型   | 默认值 |
| ----- | --------- | ------ | ------ |
| name  | icon 标识 | string | -      |
| size  | icon 大小 | number | -      |
| color | icon 颜色 | number | -      |

### Icon.Svg

| 属性     | 说明          | 类型   | 默认值 |
| -------- | ------------- | ------ | ------ |
| size     | icon 大小     | number | -      |
| color    | icon 颜色     | number | -      |
| children | 自定义 svg 图 |        | -      |

### Icon.Img

| 属性   | 说明     | 类型   | 默认值 |
| ------ | -------- | ------ | ------ |
| url    | 图片地址 | string | -      |
| width  | 图片宽度 | number | -      |
| height | 图片高度 | number | -      |
