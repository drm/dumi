import React from 'react';
import './index.less';
import * as icons from './icons';
type IconName = keyof typeof icons;
interface IconProps {
  name: IconName;
  size?: number;
  color?: string;
  [key: string]: any;
}
interface SvgProps {
  size?: number;
  color?: string;
  [key: string]: any;
}
interface ImgProps {
  url: string;
  width?: number;
  height?: number;
  [key: string]: any;
}
type IconSvgInterface = React.FC<SvgProps>;
export const IconSvg: IconSvgInterface = ({ size, color, children, ...props }) => {
  const styles = {
    fontSize: size ? size + 'px' : undefined,
    color,
  };
  return (
    <i className="__qy-icon" style={styles} {...props}>
      {children}
    </i>
  );
};
type IconImgInterface = React.FC<ImgProps>;
export const IconImg: IconImgInterface = ({ url, width, height, ...props }) => {
  const styles = {
    width,
    height,
  };
  return <img src={url} className="__qy-icon" style={styles} {...props} />;
};

interface IconInterface extends React.FC<IconProps> {
  Svg: typeof IconSvg;
  Img: typeof IconImg;
  iconNames: IconName[];
}

const Icon: IconInterface = ({ name, size, color, ...props }) => {
  const svg = icons[name];
  const styles = {
    fontSize: size ? size + 'px' : undefined,
    color,
  };
  return (
    <i
      className="__qy-icon"
      dangerouslySetInnerHTML={{ __html: svg }}
      style={styles}
      {...props}
    ></i>
  );
};
export const iconNames = Object.keys(icons) as IconName[];
Icon.Svg = IconSvg;
Icon.Img = IconImg;
Icon.iconNames = iconNames;
export default Icon;
